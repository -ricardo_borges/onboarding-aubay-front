import React from "react";
import styles from "../styles/Home.module.css";

const PaymentConfirmed = () => {
  return (
    <>
      <h1 className={styles.title}>Payment Confirmed</h1>
    </>
  );
};

export default PaymentConfirmed;
