import React, { useEffect, useState } from "react";
import useLocalStorage from "../hooks/useLocalStorage";
import styles from "../styles/Home.module.css";
import { LOCAL_STORAGE_INSTRUMENT_KEY } from "../types/Instrument";

const Reference = () => {
  const [localInstrument, setLocalInstrument] = useLocalStorage(
    LOCAL_STORAGE_INSTRUMENT_KEY,
    null
  );

  const [reference, setReference] = useState<any>(null);

  useEffect(() => {
    setReference(localInstrument.reference.fields);
  }, []);

  return (
    <>
      <h1 className={styles.title}>Reference</h1>
      {reference && Array.isArray(reference) && (
        <div>
          <p>
            {reference[0].label} {reference[0].value}
          </p>
          <p>
            {reference[1].label} {reference[1].value}
          </p>
          <p>
            {reference[2].label} {reference[2].value}
          </p>
        </div>
      )}
      <div></div>
    </>
  );
};

export default Reference;
