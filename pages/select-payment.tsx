import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import useLocalStorage from "../hooks/useLocalStorage";
import { LOCAL_STORAGE_CART_KEY, useCart } from "../providers/cart";
import { charge, createInstrument, getForm } from "../services/payment";
import styles from "../styles/Home.module.css";
import { LOCAL_STORAGE_INSTRUMENT_KEY } from "../types/Instrument";
import { LOCAL_STORAGE_ORDER_KEY } from "../types/Order";
import { PaymentTypeSchema } from "../types/PaymentTypeSchema";

const SelectPayment = () => {
  const router = useRouter();
  const [localOrder, setLocalOrder] = useLocalStorage(
    LOCAL_STORAGE_ORDER_KEY,
    {}
  );
  const [forms, setForms] = useState<PaymentTypeSchema[]>();
  const [selectedPayment, setSelectedPayment] = useState<string>();
  const [selectedFormId, setSelectedFormId] = useState<string>();
  const [selectedFormLegend, setSelectedFormLegend] = useState<string>();
  const [selectedFormFields, setSelectedFormFields] = useState<Element[]>();
  const [fieldsNames, setFieldsNames] = useState<string[]>([]);
  const [formDescription, setFormDescription] = useState<string>();
  const { cart, setCart } = useCart();
  const [localCart, setLocalCart] = useLocalStorage(LOCAL_STORAGE_CART_KEY, []);
  const [localInstrument, setLocalInstrument] = useLocalStorage(
    LOCAL_STORAGE_INSTRUMENT_KEY,
    null
  );

  useEffect(() => {
    getPaymentTypes();
    setLocalInstrument(null);
  }, []);

  useEffect(() => {
    buildForm();
  }, [selectedPayment]);

  const getPaymentTypes = async () => {
    const collection = await getForm();
    setForms(collection);
    if (Array.isArray(collection)) {
      setSelectedPayment(collection[0].id);
    }
  };

  const handleSelectPayment = async (
    event: React.FormEvent<HTMLFormElement>
  ) => {
    event.preventDefault();

    const formData = { charge_type: selectedPayment, order_id: localOrder.id };

    console.log(fieldsNames);
    fieldsNames?.forEach((item) => {
      if (item === "*") return;
      // @ts-ignore
      formData[item] = event.target[item].value;
    });

    console.log("formData", formData);

    const chargeResponse = await charge(
      formData.charge_type!,
      formData.order_id
    );

    console.log("chargeResponse", chargeResponse);

    const instrument = await createInstrument(formData);
    setLocalInstrument(instrument);

    console.log("instrument", instrument);

    // @ts-ignore
    setCart([]);
    setLocalCart([]);
    setLocalOrder({});

    if (selectedFormId === "paypal") {
      router.push(
        `https://api-test.switchpayments.com/v2/instruments/${instrument.id}/redirect`
      );
    } else if (selectedFormId === "multibanco") {
      router.push("/reference");
    } else {
      router.push("/payment-confirmed");
    }
  };

  const buildForm = () => {
    const paymentMethod = forms?.find((item) => item.id === selectedPayment);

    if (!paymentMethod) return;

    const { schema, ui_schema } = paymentMethod;

    setSelectedFormFields([]);
    setFieldsNames([]);
    setSelectedFormId(paymentMethod.id);
    setSelectedFormLegend(paymentMethod.schema.title);
    // @ts-ignore
    setFormDescription(paymentMethod.schema.description);

    const fields: any[] = [];

    const fieldsNames = paymentMethod.ui_schema["ui:order"];
    setFieldsNames(fieldsNames);

    if (paymentMethod.id === "multibanco") {
      return;
    }

    fieldsNames.forEach((value) => {
      let input;
      const property = schema.properties[value];
      // @ts-ignore
      const placeholder = ui_schema[value];
      const required =
        schema.required.findIndex((item) => item === value) > -1 ? true : false;

      if (property.type === "string") {
        // @ts-ignore
        if (property.options && Array.isArray(property.options)) {
          input = (
            <select id={property.title} name={value}>
              {
                // @ts-ignore
                property.options.map((item: any) => (
                  <option key={item.value} value={item.value}>
                    {item.label}
                  </option>
                ))
              }
            </select>
          );
        } else {
          input = (
            <input
              id={property.title}
              name={value}
              type="text"
              placeholder={placeholder && placeholder["ui:placeholder"]}
              maxLength={property.maxLength}
              minLength={property.minLength}
              required={required}
            />
          );
        }
      } else if (property.type === "integer") {
        // @ts-ignore
        const minimum = property.minimum;
        // @ts-ignore
        const maximum = property.maximum;

        input = (
          <input
            id={property.title}
            name={value}
            type="number"
            placeholder={placeholder && placeholder["ui:placeholder"]}
            min={minimum}
            max={maximum}
            required={required}
          />
        );
      }

      fields.push(
        <p key={property.title}>
          <label>
            {property.title}
            {input}
          </label>
        </p>
      );
    });

    setSelectedFormFields(fields);
  };

  return (
    <>
      <h1 className={styles.title}>Select Payment</h1>

      <div>
        <select onChange={(event) => setSelectedPayment(event.target.value)}>
          {Array.isArray(forms) &&
            forms.map((item: PaymentTypeSchema) => {
              return (
                <option key={item.id} value={item.id}>
                  {item.label}
                </option>
              );
            })}
        </select>
      </div>

      <div>
        <form
          id={selectedFormId}
          onSubmit={(event) => handleSelectPayment(event)}
        >
          <fieldset>
            <legend>{selectedFormLegend}</legend>

            <p>{formDescription}</p>

            {selectedFormFields?.map((item) => item)}
          </fieldset>

          <div>
            <button className="button" type="submit">
              Submit
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default SelectPayment;
