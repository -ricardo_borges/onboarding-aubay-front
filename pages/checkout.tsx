import { useRouter } from "next/router";
import React from "react";
import useLocalStorage from "../hooks/useLocalStorage";
import { LOCAL_STORAGE_CART_KEY } from "../providers/cart";
import { createOrder } from "../services/order";
import styles from "../styles/Home.module.css";
import { LOCAL_STORAGE_ORDER_KEY } from "../types/Order";
import { Product } from "../types/Product";

const Checkout = () => {
  const [localCart, setLocalCart] = useLocalStorage(LOCAL_STORAGE_CART_KEY, []);
  const [localOrder, setLocalOrder] = useLocalStorage(
    LOCAL_STORAGE_ORDER_KEY,
    {}
  );
  const router = useRouter();

  const handleCheckout = async () => {
    if (localCart.length === 0) {
      console.log("the cart is empty");
      return;
    }

    const createOrderDTO = {
      products: localCart.map((product: Product) => ({
        id: product.id,
        amount: 1,
      })),
    };

    const order = await createOrder(createOrderDTO);
    setLocalOrder(order);

    router.push("/select-payment");
  };

  return (
    <>
      <h1 className={styles.title}>Checkout</h1>

      <div>
        <ul>
          {localCart.map((product: Product) => (
            <li key={product.id}>
              <p className="title">
                {product.id} - {product.title} - € {product.price}x 1
              </p>
            </li>
          ))}
        </ul>
      </div>

      <div>
        <p>
          €{" "}
          {localCart.reduce(
            (acc: number, product: Product) => acc + parseFloat(product.price),
            0
          )}
        </p>
      </div>

      <div>
        <button className="button" onClick={() => handleCheckout()}>
          Checkout
        </button>
      </div>
    </>
  );
};

export default Checkout;
