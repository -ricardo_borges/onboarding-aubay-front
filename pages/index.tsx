import type { GetServerSideProps } from "next";
import React from "react";
import Cart from "../components/cart";
import ProductCard from "../components/product-card";
import { getProducts } from "../services/product";
import styles from "../styles/Home.module.css";
import { Product } from "../types/Product";

type HomeProps = {
  products: Product[];
};

const Home: React.FC<HomeProps> = ({ products }) => {
  return (
    <>
      <Cart />

      <h1 className={styles.title}>Products</h1>

      <div className={styles.grid}>
        {products.map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  try {
    const products = await getProducts();
    return {
      props: {
        products,
      },
    };
  } catch (err) {
    console.error(err);
    return {
      props: { products: [] },
    };
  }
};

export default Home;
