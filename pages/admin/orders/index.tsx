import React from "react";
import styles from "../../../styles/Home.module.css";
import { Order } from "../../../types/Order";
import { GetServerSideProps } from "next";
import { getOrders } from "../../../services/order";
import Link from "next/link";

type OrderProps = {
  orders: Order[];
};

const Orders: React.FC<OrderProps> = ({ orders }) => {
  return (
    <>
      <h1 className={styles.title}>Orders</h1>

      <div>
        <ul>
          {Array.isArray(orders) &&
            orders.map((order: Order) => (
              <li key={order.id}>
                <Link href={`/admin/orders/${order.id}`}>
                  <a className="title">
                    #{order.id} - {order.status_flow} - € {order.price}
                  </a>
                </Link>
              </li>
            ))}
        </ul>
      </div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  try {
    const orders = await getOrders();
    return {
      props: {
        orders,
      },
    };
  } catch (err) {
    console.error(err);
    return {
      props: { products: [] },
    };
  }
};

export default Orders;
