import React from "react";
import styles from "../../../styles/Home.module.css";
import { Order } from "../../../types/Order";
import { GetServerSideProps, GetServerSidePropsContext } from "next";
import { getOrder } from "../../../services/order";
import { Product } from "../../../types/Product";

type OrderProps = {
  order: Order;
};

const OrderDetails: React.FC<OrderProps> = ({ order }) => {
  return (
    <>
      <h1 className={styles.title}>Order #{order.id}</h1>

      <p>
        #{order.id} - {order.status_flow} | {order.instrument_status} - €{" "}
        {order.price}
      </p>

      <ul>
        {order.products.map((product: Product) => (
          <li key={product.id}>
            <p>
              #{product.id} - {product.title} - € {product.price} x 1
            </p>
          </li>
        ))}
      </ul>

      <div></div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const order = await getOrder(context.params!.id as string);
  console.log(order);
  return {
    props: { order },
  };
};

export default OrderDetails;
