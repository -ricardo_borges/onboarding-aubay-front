import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { useCart } from "../../providers/cart";

const Cart = () => {
  const [cartLength, setCartLength] = useState(0);
  const { cart, setCart } = useCart();

  useEffect(() => {
    setCartLength(cart ? cart.length : 0);
  });

  return (
    <div className="cart">
      <Link href="/checkout">
        <a>
          <span>{cartLength}</span>
          <Image src="/cart.png" width={50} height={50} />
        </a>
      </Link>
    </div>
  );
};

export default Cart;
