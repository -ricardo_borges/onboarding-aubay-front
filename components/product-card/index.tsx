import React, { useEffect, useState } from "react";
import Image from "next/image";
import { Product } from "../../types/Product";
import { LOCAL_STORAGE_CART_KEY, useCart } from "../../providers/cart";
import useLocalStorage from "../../hooks/useLocalStorage";

type ProductCardProps = {
  product: Product;
};

const ProductCard: React.FC<ProductCardProps> = ({ product }) => {
  const [buttonText, setButtonText] = useState("Add to Cart");
  const { cart, setCart } = useCart();
  const [localCart, setLocalCart] = useLocalStorage(LOCAL_STORAGE_CART_KEY, []);

  useEffect(() => {
    if (Array.isArray(localCart)) {
      const index = localCart.findIndex((item) => item.id === product.id);
      if (index > -1) setButtonText("Remove from Cart");
    }
  }, []);

  const addProduct = (product: Product) => {
    if (Array.isArray(cart)) {
      // @ts-ignore
      setCart([...cart, product]);
      // @ts-ignore
      setLocalCart([...cart, product]);
    } else {
      // @ts-ignore
      setCart([product]);
      setLocalCart([product]);
    }
    setButtonText("Remove from Cart");
  };

  const removeProduct = (index: number) => {
    cart!.splice(index, 1);
    // @ts-ignore
    setCart([...cart]);
    // @ts-ignore
    setLocalCart([...cart]);
    setButtonText("Add to Cart");
  };

  const handleAddToCart = (product: Product) => {
    if (Array.isArray(cart)) {
      const index = cart.findIndex((item) => item.id === product.id);

      if (index > -1) {
        removeProduct(index);
      } else {
        addProduct(product);
      }
    } else {
      addProduct(product);
    }
  };

  return (
    <div className="card">
      <Image src={product.image} width={250} height={250} />
      <h2>
        {product.title} {product.id}
      </h2>
      <p>{product.description.substring(0, 50)}...</p>
      <div className="card-actions">
        <span>€ {product.price}</span>
        <button onClick={() => handleAddToCart(product)}>{buttonText}</button>
      </div>
    </div>
  );
};

export default ProductCard;
