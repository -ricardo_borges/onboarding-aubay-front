import React, {
  ReactElement,
  useEffect,
  useState,
  createContext,
  useContext,
} from "react";
import useLocalStorage from "../hooks/useLocalStorage";

export const LOCAL_STORAGE_CART_KEY = "cart";

// export type Cart = Product[];

type CartProviderProps = {
  children?: ReactElement;
};

type UseCart = {
  cart: any[];
  setCart: (cart: any[]) => void;
};

export const CartContext = createContext<Partial<UseCart>>({
  cart: [],
  setCart: (cart) => console.warn("no cart provider"),
});

export const CartProvider: React.FC<CartProviderProps> = (
  props: CartProviderProps
) => {
  const [cart, setCart] = useState([]);
  const [localCart] = useLocalStorage(LOCAL_STORAGE_CART_KEY, []);

  useEffect(() => {
    setCart(localCart);
  }, []);

  return (
    // @ts-ignore
    <CartContext.Provider value={{ cart, setCart }}>
      {props.children}
    </CartContext.Provider>
  );
};

export const useCart = () => useContext(CartContext);
