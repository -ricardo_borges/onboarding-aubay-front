FROM node:16

WORKDIR /usr/src/app

ENV API_URL=https://086f-2001-818-eb30-1100-53f6-ff33-88fe-8a9f.ngrok.io

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE 3000

CMD [ "npm", "start" ]
