export type Instrument = {
  id: string;
  status: string;
};

export const LOCAL_STORAGE_INSTRUMENT_KEY = "instrument";
