export type Product = {
  id: number;
  title: string;
  description: string;
  image: string;
  category: string;
  price: string;
};
