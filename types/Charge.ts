export type Charge = {
  id: string;
  amount: number;
  charge_type: string;
  charge_type_label: string;
  confirmed: boolean;
  created_at: string;
  currency: string;
  events_url: string;
};
