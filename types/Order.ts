import { Product } from "./Product";

export type Order = {
  id: number;
  description: string;
  status_flow: string;
  instrument_status: string;
  price: string;
  products: Product[];
};

export const LOCAL_STORAGE_ORDER_KEY = "order";
