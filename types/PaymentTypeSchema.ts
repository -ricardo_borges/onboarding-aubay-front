export type SchemaProperty = {};

export type PaymentTypeSchema = {
  id: string;
  label: string;
  payout: boolean;
  capture_on_creation: boolean;
  schema: {
    properties: {
      cvc: {
        type: "string";
        title: "CVV";
        minLength: 3;
        maxLength: 4;
      };
      expiration_month: {
        type: "integer";
        title: "Expiration Month";
        minimum: 1;
        maximum: 12;
      };
      expiration_year: {
        type: "integer";
        title: "Expiration Year";
      };
      name: {
        type: "string";
        title: "Cardholder Name";
        minLength: 3;
        maxLength: 255;
      };
      number: {
        type: "string";
        title: "Card Number";
        minLength: 14;
        maxLength: 19;
      };
    };
    title: "Credit/Debit Card";
    type: "object";
    required: ["number", "cvc", "expiration_month", "expiration_year", "name"];
  };
  schema_merchant: {
    properties: {
      enable3ds: {
        type: "boolean";
        title: "Enable 3DS";
      };
    };
    type: "object";
    required: [];
  };
  ui_schema: {
    "ui:charge_type_customization": "card";
    "ui:order": [
      "name",
      "number",
      "expiration_month",
      "expiration_year",
      "cvc"
    ];
    number: {
      "ui:placeholder": "1111 2222 3333 4444";
    };
    name: {
      "ui:placeholder": "Full Name";
    };
    cvc: {
      "ui:placeholder": "123";
      "ui:help": "Last 3 digits on the back of the card";
    };
  };
};
