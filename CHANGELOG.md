# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## Ongoing Changes

### Added

## [1.2.0]

### Added

- Display form
- Create charge
- Create Instrument

## [1.1.0]

### Added

- Checkout page
- Orders page
- Order detail page

## [1.0.0]

### Added

- List available products
- Cart functionality
