import { Charge } from "../types/Charge";
import { Instrument } from "../types/Instrument";
import { api } from "./api";

export type CreateInstrumentDTO = {
  order_id: number;
  cvc: string;
  expiration_month: string;
  expiration_year: string;
  name: string;
  number: string;
};

export const getForm = async (): Promise<any[]> => {
  const { data } = await api.get("v1/payment/methods");
  return data.collection;
};

export const charge = async (
  charge_type: string,
  order_id: number
): Promise<Charge> => {
  const { data } = await api.post("/v1/payment/charge/", {
    charge_type,
    order_id,
  });

  return data;
};

export const createInstrument = async (body: Object): Promise<Instrument> => {
  const { data } = await api.post("/v1/payment/instrument/", {
    ...body,
  });

  return data;
};
