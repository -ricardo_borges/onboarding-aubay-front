import { Order } from "../types/Order";
import { api } from "./api";

export type CreateOrderDTO = {
  products: { id: number; amount: number }[];
};

export const createOrder = async (
  createOrderDTO: CreateOrderDTO
): Promise<Order> => {
  const { data } = await api.post("/v1/orders/create/", createOrderDTO);
  return data;
};

export const getOrders = async (): Promise<Order[]> => {
  const { data } = await api.get("/v1/orders");

  return data.map((order: Order) => ({
    id: order.id,
    price: order.price,
    status: order.status_flow,
    description: order.description,
    products: order.products,
    instrument_status: order.instrument_status,
  }));
};

export const getOrder = async (id: string): Promise<Order> => {
  const { data } = await api.get(`/v1/orders/${id}`);

  return {
    id: data.id,
    price: data.price,
    status_flow: data.status_flow,
    description: data.description,
    products: data.products,
    instrument_status: data.instrument_status,
  };
};
