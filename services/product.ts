import { Product } from "../types/Product";
import { api } from "./api";

export const getProducts = async (): Promise<Product[]> => {
  const { data } = await api.get("/v1/products");
  return data.map((product: any) => ({
    id: product.id,
    title: product.title,
    price: product.price,
    category: product.category,
    description: product.description,
    image: product.image,
  }));
};
